package se325.lab04.concert.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se325.lab04.concert.domain.Concert;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;


/**
 * Web service resource implementation for the Concert application. An instance of this
 * class handles all the HTTP requests for the Concert Web Service
 */
@Path("/concerts")
public class ConcertResource {

    private static Logger LOGGER = LoggerFactory.getLogger(ConcertResource.class);

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getConcert(@PathParam("id") Long id){
        // Acquire an EntityManager (creating a new persistence context).
        EntityManager em = PersistenceManager.instance().createEntityManager();
        Concert concert;
        try {

            // Start a new transaction.
            em.getTransaction().begin();

            concert = em.find(Concert.class, id);

            // Commit the transaction.
            em.getTransaction().commit();

        } finally {
            // When you're done using the EntityManager, close it to free up resources.
            em.close();
        }
        Response response;
        if(concert == null){
            response = Response.status(Response.Status.NOT_FOUND).build(); // 404 Not Found
        } else {
            response =  Response.ok(concert).build(); // 200 OK
        }

        return response;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createConcert(Concert concert){
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(concert);
            em.flush(); // flush to ensure that the ID has been created for the object

            em.getTransaction().commit();

        }
        finally {
            em.close();
        }

        return Response.created(URI.create("/concerts/" + concert.getId())).build(); // 201 Created
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateConcert(Concert concert){
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try {
            em.getTransaction().begin();
            em.merge(concert);
            em.getTransaction().commit();
        } catch (IllegalArgumentException e){
            return Response.status(Response.Status.NOT_FOUND).build(); // 404 Not Found
        } finally {
            em.close();
        }

        return Response.status(Response.Status.NO_CONTENT).build(); // 204 No Content
    }

    @DELETE
    @Path("{id}")
    public Response deleteConcert(@PathParam("id") Long id){
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try {
            em.getTransaction().begin();
            Concert concert = em.find(Concert.class, id);
            if(concert == null){
                throw new IllegalArgumentException();
            }
            em.remove(concert);
            em.getTransaction().commit();
        } catch (IllegalArgumentException e){
            return Response.status(Response.Status.NOT_FOUND).build(); // 404 Not Found
        } finally {
            em.close();
        }
        return Response.status(Response.Status.NO_CONTENT).build(); // 204 No Content
    }

    @DELETE
    public Response deleteAllConcerts(){
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try {
            em.getTransaction().begin();
            TypedQuery<Concert> concertQuery = em.createQuery("select c from Concert c", Concert.class);
            List<Concert> concerts = concertQuery.getResultList();
            for(Concert concert: concerts){ // iterate through all Concerts and remove
                em.remove(concert);
            }
            em.getTransaction().commit();
        } finally {
            em.close();
        }

        return Response.status(Response.Status.NO_CONTENT).build(); // 204 No Content
    }

}
