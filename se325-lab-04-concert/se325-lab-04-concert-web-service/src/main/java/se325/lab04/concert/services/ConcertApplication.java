package se325.lab04.concert.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Application subclass for the Concert Web service.
 */
@ApplicationPath("/services")
public class ConcertApplication extends Application {

    private Set<Object> singletonInstances = new HashSet<>();
    private Set<Class<?>> singletonClasses = new HashSet<>();

    public ConcertApplication(){
        // Register the ConcertResource class to handle the HTTP requests. Resource-per-request model is used. This class is instantiated every time a request comes in
        // this allows the application to be stateless with an underlying database.
        singletonClasses.add(ConcertResource.class);
        // Register the PersistenceManager object that provides the means to create an EntityManager
        singletonInstances.add(new PersistenceManager());
    }

    @Override
    public Set<Object> getSingletons(){
        return singletonInstances;
    }

    @Override
    public Set<Class<?>> getClasses(){
        return singletonClasses;
    }
}
