package se325.lab04.concert.domain;

public enum Genre {
    Pop, HipHop, RhythmAndBlues, Acappella, Metal, Rock
}